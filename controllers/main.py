# -*- coding: utf-8 -*-

from odoo.http import request
from odoo.addons.sale_quotation_api.controllers import main


class Main(main.Main):
    def _prepare_order_vals(self, ref, date, customerId, lines):
        vals = super(Main, self)._prepare_order_vals(ref, date, customerId, lines)
        workflow_id = request.env.ref('sale_automation.sale_automatic_validation')
        vals['workflow_process_id'] = workflow_id.id
        return vals

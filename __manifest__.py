# -*- coding: utf-8 -*-
{
    'name': "Automatización de Ventas",
    'summary': "Flujo automático. Permite confirmar pedidos, picking y validar facturas",
    'author': "Roberto Sierra <roberto@ideadigital.com.ar>",
    'website': "https://gitlab.com/r-sierra/sale-automation",
    'category': 'Sales/Sales',
    'version': '0.1',
    'depends': [
        'sale_automatic_workflow',
        'sale_automatic_workflow_cancel',
        'l10n_ar_sale_automatic_workflow_payment',
        'sale_quotation_api',
    ],
    'data': [
        'data/sale_automation_data.xml',
    ],
    'license': 'LGPL-3',
}
